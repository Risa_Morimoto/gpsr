#!/usr/bin/env python
# -*- coding: utf-8 -*-

#音声班 GPSRカテゴリ2
#解析して結果をPublishするプログラム
#メッセージ型  String型(取ってくるアイテム)
#           String型(取ってくる場所)

import rospy
import sys
from std_msgs.msg import String
import subprocess
import treetaggerwrapper
import re
from ros_start.msg import gpsr
from ros_start.msg import gpsr2
import os
from time import sleep
import datetime

reload(sys)

sys.setdefaultencoding('utf-8')

name_dict = {}
place_dict = {}
before_time = 0

def name_dictionary():  #アイテムのリストを作成
    with open('/home/risa/catkin_ws/src/ros_start/gpsr/name.txt', 'r') as f: #pythonでのファイルオープン,パスは絶対パス
        name_list = f.readlines()  #1行ずつ処理を行う
        for name in name_list:
            name = name.rstrip().decode('utf-8').split(',') #改行コードを削除し，デコードした後カンマで質問と解答に区切る           
            name_dict.setdefault(name[0], []).append(name[1])

def place_dictionary(): #場所のリストを作成
    with open('/home/risa/catkin_ws/src/ros_start/gpsr/place.txt', 'r') as f:
        place_list = f.readlines()
        for place in place_list:
            place = place.rstrip().decode('utf-8').split(',')           
            place_dict.setdefault(place[0], []).append(place[1])

def get_noun(data):  #文字列を形態素解析して名詞のみを抜き出す
    tagger = treetaggerwrapper.TreeTagger(TAGLANG='en',TAGDIR='./tree-tagger-install') #TAGDIRにはTreeTaggerをインストールしたディレクトリを指定。
    tags = tagger.TagText(data.decode('utf-8'))  #形態素解析
    noun = []
    for tag in tags:
        qa = tag.rstrip().decode('utf-8').split('\t')    #形態素解析結果を分割
        if qa[1] == 'NN':                            
            noun.append(qa[0])
    return noun


def callback(message):
    now = datetime.datetime.today()  #時刻を取得
    input = message.word
    count = message.count

    print now
    
    print input
    print count

    if count == 0 and "Bring" in input:

        before_time = now.second            

        noun1 = get_noun(input)
        length1 = len(noun1)  #要素数を取得

        if length1 == 2:
            items = noun1[0]
            places = noun1[1]
            speak1 = "Bring me some " + items + " from a " + places                
        if length1 == 1:   #名詞が1つしかない(場所か物が不足)
            if "some from" in input:
                items = "Snack"
                places = noun1[0]
            else:
                items = noun1[0]
                places = "seating"
            speak1 = "Bring me some " + items + " from a " + places
        if length1 == 0:  #名詞が1つもない(場所と物両方不足)
            items = "Snack"
            places = "seating"
            speak1 = "Bring me some " + items + " from a " + places

        os.system('espeak "{}" -s 100'.format(speak1))  #問題文を復唱

        sleep(3)

        speak2 = "Which " + items + " should I bring?"
        os.system('espeak "{}" -s 100'.format(speak2)) #質問を発話
            
    if count == 1 and "is" in input:

        time = now.second - before_time

        if time > 10:

            before_time = now.second

            noun2 = get_noun(input)
            length2 = len(noun2)

            if length2 == 2:          #publishするアイテムを決定
                if noun2[0] == items:
                    items = noun2[1]
                else:
                    items = name_dict[items]
            if length2 == 1:
                if noun2[0] == items:
                    items = name_dict[items]
                else:
                    items = noun2[0]
            if length2 == 0:        
                items = name_dict[items]
            
            sleep(2)

            speak3 = "Which " + places + " should I bring from?"
            os.system('espeak "{}" -s 100'.format(speak3))  #質問を発話  

                            
    if count == 2 and "is" in input: 
            
        time = now.second - before_time
                    
        if time > 10:
                    
            before_time = now.second

            noun3 = get_noun(input)
            length3 = len(noun3)

            if length3 == 2:
                if noun3[0] == places:
                    places = noun3[1]
                else:
                    places = place_dict[places]
            if length3 == 1:
                if noun3[0] == places:
                    places = place_dict[places]
                else:
                    places = noun3[0]
            if length3 == 0:        
                places = place_dict[places]

            msg = gpsr2()
            msg.place = places
            msg.name = items
            gpsr_pub.publish(msg) 


if __name__ == "__main__":
    rospy.init_node('listener')
    sub = rospy.Subscriber('chatter',gpsr,callback)
    name_dictionary()
    place_dictionary()     
    gpsr_pub = rospy.Publisher('gpsr',gpsr2, queue_size=10)
    rospy.spin()  #ここから下に書いても実行されない
