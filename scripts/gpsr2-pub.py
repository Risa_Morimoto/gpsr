#!/usr/bin/env python
# -*- coding: utf-8 -*-

#音声班 GPSRカテゴリ2
#Juliusで質問文(の一部)を入力してpublishするプログラム
#メッセージ型 string型(juliusから返ってくる音声認識結果)
#          int型(publishした回数-1)

import rospy
import socket
import re
from std_msgs.msg import String
from time import sleep
from ros_start.msg import gpsr
 
HOST = "localhost" # アドレス
PORT = 10500 # ポート
 
# TCPクライアントを作成し接続
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect((HOST, PORT))

rospy.init_node('talker')
pub = rospy.Publisher('chatter',gpsr,queue_size=10)

r = rospy.Rate(5)
num = 0
while not rospy.is_shutdown():
    # サーバの応答を受信
    while 1:
        try:
            data = ""
            while 1:
                response = client.recv(1024)
                if '<RECOGOUT>\n' in response and '</RECOGOUT>\n' in response:
                    data = response
                if '<RECOGOUT>\n' in response or '</RECOGOUT>\n' in response:
                    data = data + response
                #else:
                #    data = ""

                #if '</RECOGOUT>' in data:
                text = ""
                line_list = data.split('\n')
                #print line_list
                for line in line_list:
                    #print line
                    if 'WHYPO' in line:
                        word = re.compile('WORD="((?!").)+"').search(line)
                        if word:
                            text = text + ' ' + word.group().split('"')[1]
                            #print text

                if text is not "":
                    msg = gpsr()     
                    slice = text.replace('<s>','')
                    msg.word = slice
                    msg.count = num
                    print slice
                    pub.publish(msg)
                    num += 1                     
                data = ""

        except KeyboardInterrupt:
            #例外が発生した場合はクライアントを一旦消し，3秒後にクライアントを再作成・再接続する
            client.close()
            sleep(3)
            client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            client.connect((HOST, PORT))
  
    r.sleep()

    
